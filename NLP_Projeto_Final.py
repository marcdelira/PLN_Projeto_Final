﻿# Importar pacotes
from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer
import os
#import nltk
#
#nltk.download('movie_reviews')

lingua = ''
while (lingua != 'en'):
   # limpando a tela
   os.system('cls')
   # Solictando frase ao usuário
   frase = input('Digite uma frase em inglês para ser traduzida para português ou "sair" para abandonar o programa: ')
   if (frase == 'sair'):
       break
   # Transformando a frase em um tipo textblob.blob.TextBlob
   var_blob = TextBlob(frase)
   # Verificando se a frase está em inglês
   lingua = var_blob.detect_language()
   if lingua != 'en':
      print('A frase informada não está em inglês')
      input('Pressione [ENTER] para continuar')

if frase.lower() != 'sair':
   # Apresentando a tradução da frase
   print('A tradução da frase em inglês ===> ', frase, ' - para português é ===> ', var_blob.translate(to="pt"))

   # Executando análise de sentimento para a frase informada:
   # O padrão da biblioteca é utilizar o analisador "PatternAnalyzer" (tem por base a biblioteca do pattern).
   # O outro analisador é o "NaiveBayesAnalyzer" (é um classificador do NLTK treinado sobre um corpus de resenhas de filmes).
   # Vamos executar os dois para ver se existem diferenças significativas.
   # A análise de sentimento, também chamada de mineração de opinião tenta determinar se um texto é objetivo ou subjetivo, positivo ou negativo.
   # Para a nossa biblioteca (Pattern) a determinação é feita com base na utilização de adjetivos.
   # A polaridade mede um índice entre negativo e positivo que varia de -1.0 até +1.0.
   # A subjetividade mede um índice entre objetivo e subjetivo que varia de 0.00 até 1.0.

   # Executando com o default ("PatternAnalyzer")
   print('Análise de sentimento da frase com o PatternAnalyzer (sentiment): ', var_blob.sentiment)

   # Executando com o "NaiveBayesAnalyzer"
   var_blob_2 = TextBlob(frase, analyzer=NaiveBayesAnalyzer())
   print('Análise de sentimento da frase com o NaiveBayesAnalyzer (sentiment): ', var_blob_2.sentiment)

# ===================================== FIM DO PROGRAMA ===================================


